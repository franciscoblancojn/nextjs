import Container from "../components/container";

const index = () =>{
    return (
        <Container>
            <h1>Hello word</h1>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere illo aut nihil. Pariatur omnis, consequuntur, inventore doloribus fuga reprehenderit voluptatem distinctio dolor eos esse rem nesciunt blanditiis quas corporis dolore.</p>
        </Container>
    )
}

export default index;