import Link from 'next/link'

const header = () =>{
    return (
        <div>
            <h2>Header</h2>
            <ul>
                <li>
                    <Link href="/">
                        <a>Inicio</a>
                    </Link>
                </li>
                <li>
                    <Link href="/service">
                        <a>Service</a>
                    </Link>
                </li>
            </ul>
        </div>
    )
}

export default header;