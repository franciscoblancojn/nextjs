import Header from "./header";
import Head from 'next/head';
const Container = (pros) =>{
    return (
        <div>
            <Head>
                <title>
                    Next
                </title>
            </Head>
            <Header/>
            {pros.children}
        </div>
    )
}

export default Container;